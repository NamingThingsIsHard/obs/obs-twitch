import logging
import sys

from PyQt5.QtWidgets import QApplication

from view.obsttvmain import QObsTtvMain

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    app = QApplication(sys.argv)
    window = QObsTtvMain()
    window.show()
    sys.exit(app.exec_())
