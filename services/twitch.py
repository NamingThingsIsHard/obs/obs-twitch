from twitch import TwitchClient

from .service import Service


class TwitchService(Service):
    """
    :type client: TwitchClient | None
    """

    def start(self, client_id=None, oauth_token=None):
        if self.is_connected:
            self.connected.emit()
            return
        client = TwitchClient(
            client_id=client_id,
            oauth_token=oauth_token
        )
        try:
            client.users.get()
            self.client = client
        except Exception as e:
            raise ConnectionError(e)
        self.connected.emit()

    def stop(self):
        if self.client:
            self.client = None
        self.disconnected.emit()


service = TwitchService()
