from obswebsocket import obsws

from .service import Service


class ObsService(Service):

    def start(self, host='localhost', port=4444, password=''):
        client = obsws(host=host, port=port, password=password)
        try:
            client.connect()
            self.client = client
        except Exception as e:
            raise ConnectionError(e)
        self.connected.emit()

    def stop(self):
        if self.client:
            self.client.disconnect()
            self.client = None
        self.disconnected.emit()


service = ObsService()
