from logging import getLogger

from PyQt5.QtCore import pyqtSignal, QObject


class Service(QObject):
    connected = pyqtSignal()
    disconnected = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.client = None
        self.logger = getLogger(self.__class__.__name__)

    def start(self, **kwargs):
        pass

    def stop(self):
        pass

    @property
    def is_connected(self):
        return self.client is not None
