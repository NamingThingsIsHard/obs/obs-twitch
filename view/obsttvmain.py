from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeySequence, QIntValidator
from PyQt5.QtWidgets import QMainWindow, QAction, QDialog, QVBoxLayout, QLineEdit, QWidget, QLabel, \
    QPushButton
from obswebsocket import obsws
from qsettingswidget import QSettingsWidget

from services import twitch_service
from view.password_form_group import QPasswordFormGroup


class QObsTtvMain(QMainWindow):
    ORGANIZATION = "NamingThingsIsHard"
    APP_NAME = "OBS TTV Plugin"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._create_menus()
        self.setWindowTitle(self.APP_NAME)
        self.client = obsws()

        self.settings_widget = QSettingsWidget(settings_args=[
            self.ORGANIZATION,
            self.APP_NAME
        ])
        self.settings_widget.converters.append(QPasswordFormGroup.Converter)
        self._init_settings()

        self.settings_dialog = QDialog()
        self._init_settings_dialog()

        self.main_widget = QWidget()
        self._init_main_widget()

        # Set the central widget of the Window. Widget will expand
        # to take up all the space in the window by default.
        self.setCentralWidget(self.main_widget)

    def _init_main_widget(self):
        layout = QVBoxLayout()
        self.main_widget.setLayout(layout)

        label = QLabel(self.tr("OBS TTV"))
        label.setAlignment(Qt.AlignCenter)
        layout.addWidget(label)

        self.connect_button = QPushButton(self.tr("Connect to OBS"))
        self.connect_button.clicked.connect(self._on_connect_clicked)
        layout.addWidget(self.connect_button)

        self.twitch_button = QPushButton(self.tr("Test Twitch settings"))
        self.twitch_button.clicked.connect(self._on_test_twitch_clicked)
        layout.addWidget(self.twitch_button)

    def _on_connect_clicked(self):
        self.connect_to_obs()

    def _on_test_twitch_clicked(self):
        try:
            twitch_service.start(
                self.settings_widget.settings.value("main/twitch/clientID"),
                self.settings_widget.settings.value("main/twitch/oauthToken"),
            )
            self.statusBar().showMessage(self.tr("Twitch test successful"))
        except Exception as e:
            self.statusBar().showMessage(self.tr("Couldn't connect to twitch: %s" % e))

    def _create_menus(self):
        self.toolsMenu = self.menuBar().addMenu("&Tools")
        settings_action = QAction(self.tr("Settings"), self)
        settings_action.setStatusTip(self.tr("Open the settings"))
        settings_action.triggered.connect(self._open_settings)
        settings_action.setShortcuts(QKeySequence(Qt.CTRL + Qt.Key_P))
        self.toolsMenu.addAction(settings_action)

    def _open_settings(self):
        self.settings_dialog.show()

    def _init_settings_dialog(self):
        self.settings_dialog = QDialog(self)
        self.settings_dialog.setWindowTitle(self.tr("Settings"))
        self.settings_dialog.setLayout(QVBoxLayout())
        self.settings_dialog.layout().addWidget(self.settings_widget)

        save_button = QPushButton(self.tr("Save"))
        save_button.clicked.connect(lambda: self.settings_widget.save_settings())

        self.settings_dialog.layout().addWidget(save_button)

    def _init_settings(self):
        port_widget = QLineEdit()
        port_widget.setValidator(QIntValidator(0, 65536))
        self.settings_widget.continue_group("main")
        self.settings_widget.add_field(
            self.tr("Port"),
            port_widget, "4444",
            self.tr("OBS Port")
        )

        self.settings_widget.begin_group("twitch", self.tr("Twitch"))
        self.settings_widget.add_field("clientID", QLineEdit(), ui_string=self.tr("Client ID"))
        self.settings_widget.add_field("oauthToken", QPasswordFormGroup(), ui_string=self.tr("OAuth token"))
        self.settings_widget.end_group()
        self.settings_widget.end_group()

        self.settings_widget.build_ui()
        self.settings_widget.load_settings()

    def showEvent(self, event):
        self.connect_to_obs()
        super(QObsTtvMain, self).showEvent(event)

    def connect_to_obs(self):
        # Initialize connection to OBS when we start the application
        if self.client.thread_recv is None or not self.client.thread_recv.running:
            try:
                self.client.connect(port=self.settings_widget.settings.value("Port"))
                self.statusBar().showMessage(self.tr("Connection to OBS successful"))
                # TODO: Listen to events on websocket and disable when there's an error
                self.connect_button.setDisabled(True)
            except:
                self.connect_button.setDisabled(False)
                self.statusBar().showMessage(self.tr("Couldn't connect to OBS"))

    def destroyed(self, p_object=None):
        self.client.disconnect()
        super(QObsTtvMain, self).destroyed(p_object)
