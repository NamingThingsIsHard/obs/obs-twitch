from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QLineEdit, QPushButton
from qsettingswidget.converters.converter import Converter, Signals


class QPasswordFormGroup(QWidget):

    def __init__(self):
        super().__init__()

        self.layout = QGridLayout(self)

        self._q_password = QLineEdit()
        self._q_password.setEchoMode(QLineEdit.Password)

        self._q_toggle = QPushButton("Show/Hide")

        self.layout.addWidget(self._q_password, 0, 1, 1, -1)
        self.layout.addWidget(self._q_toggle, 1, 1, 1, -1)

        self._q_toggle.clicked.connect(self._toggle_hide_clicked)

        # Signals
        self.textChanged = self._q_password.textChanged
        self.changed = pyqtSignal(str)

    def text(self):
        return self._q_password.text()

    def setText(self, string):
        self._q_password.setText(string)

    def _toggle_hide_clicked(self):
        if self._q_password.echoMode() == QLineEdit.Normal:
            self._q_password.setEchoMode(QLineEdit.Password)
        else:
            self._q_password.setEchoMode(QLineEdit.Normal)

    class Converter(Converter):
        @classmethod
        def get(cls, widget):
            return widget.text()

        @classmethod
        def set(cls, widget, value):
            widget.setText(value)

        @classmethod
        def signal(cls, widget, signal_enum):
            assert (signal_enum == Signals.CHANGED)
            return widget.textChanged

        @classmethod
        def can_convert(cls, widget):
            return isinstance(widget, QPasswordFormGroup)
