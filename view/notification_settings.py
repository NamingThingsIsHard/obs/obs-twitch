from PyQt5.QtWidgets import QWidget, QLabel, QGroupBox, QFormLayout, QVBoxLayout


class QNotificationSettings(QWidget):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.layout = QVBoxLayout()
        self._q_follower_group = QNotificationSettingsGroup("Followers")

        self.layout.addWidget(self._q_follower_group)
        self.setLayout(self.layout)


class QNotificationSettingsGroup(QGroupBox):

    def __init__(self, title, parent=None):
        super().__init__(title, parent)

        self._q_media_layout = QFormLayout()
        self._q_media_layout.addRow("Media source", QLabel("Media source"))
        self._q_media_layout.addRow("Text source", QLabel("Text source"))
        self.setLayout(self._q_media_layout)

        self._q_media_selection = None
        self._q_text_selection = None
